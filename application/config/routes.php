<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';

$route['index'] = 'home';
$route['about-us'] = 'home/About';
$route['contact-us'] = 'home/ContactUs';
$route['services'] = 'home/Services';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



