<?php
$menu_active = "services";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/titleLogo.png'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css.map'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/genel.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css?v=123'); ?>" />
    <script src='<?php echo base_url('assets/js/jquery-3.5.1.js'); ?>'></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.js'); ?>"></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <title>Services</title>
</head>

<body class="services">
    <div class="specialNav">
        <?php include("layout/menu.php"); ?>
    </div>
    <header class="servicesHeader">
        <div class="hoodArea">
            <div class="container">
                <div class="row">
                    <div class="line"></div>
                    <p class="hoodB">Services <br> Aytim Radgiving</p>
                    <p class="hoodS">Professional and legally secure advice in aliens law</p>
                </div>
            </div>
        </div>
    </header>
    <section class="items">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-4 col-xxl-4 mt-5 ">
                    <div class="card">
                        <div class="cardHoodArea">
                            <img class="img-fluid" src="<?php echo base_url('assets/img/icon/1.svg'); ?>" alt="">
                        </div>
                        <div class="textAndButton">
                            <hr class="cardHr" />
                            <p>
                                Rådgivning
                            </p>
                            <button>Get Information</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-4 col-xxl-4 mt-5 ">
                    <div class="card">
                        <div class="cardHoodArea">
                            <img class="img-fluid" src="<?php echo base_url('assets/img/icon/2.svg'); ?>" alt="">
                        </div>
                        <div class="textAndButton">
                            <hr class="cardHr" />
                            <p>
                                Ny ansökan om arbetstillstånd från utlandet
                            </p>
                            <button>Get Information</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-4 col-xxl-4 mt-5 ">
                    <div class="card">
                        <div class="cardHoodArea">
                            <img class="img-fluid" src="<?php echo base_url('assets/img/icon/3.svg'); ?>" alt="">
                        </div>
                        <div class="textAndButton">
                            <hr class="cardHr" />
                            <p>
                                Ansökan om arbetstillstånd efter att du fått avslag på din
                                ansökan om asyl
                            </p>
                            <button>Get Information</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-4 col-xxl-4 mt-5">
                    <div class="card">
                        <div class="cardHoodArea">
                            <img class="img-fluid" src="<?php echo base_url('assets/img/icon/4.svg'); ?>" alt="">
                        </div>
                        <div class="textAndButton">
                            <hr class="cardHr" />
                            <p>
                                Ansökan om arbetstillstånd efter att du fått avslag på din
                                ansökan om asyl
                            </p>
                            <button>Get Information</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-4 col-xxl-4 mt-5">
                    <div class="card">
                        <div class="cardHoodArea">
                            <img class="img-fluid" src="<?php echo base_url('assets/img/icon/5.svg'); ?>" alt="">
                        </div>
                        <div class="textAndButton">
                            <hr class="cardHr" />
                            <p>
                                Ansökan om arbetstillstånd
                                vid byte av yrke eller
                                arbetsgivare
                            </p>
                            <button>Get Information</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-4 col-xxl-4 mt-5">
                    <div class="card">
                        <div class="cardHoodArea">
                            <img class="img-fluid" src="<?php echo base_url('assets/img/icon/6.svg'); ?>" alt="">

                        </div>
                        <div class="textAndButton">
                            <hr class="cardHr" />
                            <p>
                                Ansökan om arbetstillstånd
                                vid byte av yrke eller
                                arbetsgivare

                            </p>
                            <button>Get Information</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-4 col-xxl-4 mt-5">
                    <div class="card">
                        <div class="cardHoodArea">
                            <img class="img-fluid" src="<?php echo base_url('assets/img/icon/7.svg'); ?>" alt="">

                        </div>
                        <div class="textAndButton">
                            <hr class="cardHr" />
                            <p>
                                Översätta diverse
                                handlingar
                            </p>
                            <button>Get Information</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contactUsArea">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-9 textArea">
                    <p class="hoodB">Are You Looking For Visa Applications Just Call Us!</p>
                    <p class="hoodS">Call us today <span> <a href="tel:1-888-45678">1-888-45678 </a></span>or Email us: <a href="mailto:info@domainname.com">info@domainname.com</a></p>
                </div>
                <div class="col-12 col-lg-3 contactButtonArea">
                    <a href="">Contact Us</a>
                </div>
            </div>
        </div>
    </section>
    <section class="sectionFour">
        <div class="container whatOurClientSays">
            <div class="row">
                <div class="col-12 col-lg-6 order-2 order-lg-1 p-0 whatOurClientSaysImg">
                    <img class="img-fluid" src="<?php echo base_url('assets/img/bigImage.png'); ?>" alt="">
                </div>
                <div class="col-12 col-lg-6 order-1  order-lg-2  whatOurClientSaysText bg-white p-4">
                    <div class="hoodArea">
                        <p class="hoodS">
                            TESTIMONIALS
                        </p>
                        <p class="hoodB">
                            What Our <span class="hoodBTwo">Client Says?</span>
                        </p>
                        <div class="textArea">
                            <p class="text">
                                I really would like to appreciate Tripzia and the
                                entire team, especially Ms Anandias Alex for
                                helping me get my student visa for the Brunel
                                University. She helped me all way to find the
                                University & right course. She was there for
                                me through the entire process.
                            </p>
                            <img src="<?php echo base_url('assets/img/MaskGroup3.png'); ?>" alt="">
                            <p class="name">Alan Sears</p>
                            <p class="place">Switzerland</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="prices padding-t-100 padding-b-100">
        <div class="container">
            <div class="row">
                <div class="col-12 pricesHoods">
                    <p class="hoodS">PERMIZON OCH TREDJE PART</p>
                    <p class="hoodB">Priser och avgifter</p>
                </div>
                <div class="col-12 pricesArea">
                    <div class="textArea">
                        <p>NY ansökan av arbetstillstand</p>
                        <p>22 000 kr</p>
                    </div>
                    <div class="textArea">
                        <p>Förlangningsansökan av arbetstillstand</p>
                        <p>26 000 kr</p>
                    </div>
                    <div class="textArea">
                        <p>byte av arbetsgivare eller yrke</p>
                        <p>26 000 kr</p>
                    </div>
                    <div class="textArea">
                        <p>Sparbyte studenttillstand till arbetstillstand</p>
                        <p>24 000 kr</p>

                    </div>
                    <div class="textArea">
                        <p>Arbetstillstand för asylsökande</p>
                        <p>26 000 kr</p>

                    </div>
                    <div class="textArea">
                        <p>Ansökan om permanent arbetstillstand</p>
                        <p>28 000 kr</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <?php include("layout/footer.php"); ?>
    </footer>
</body>
<script>
    document.getElementById('iframe').style.zIndex = 1;
</script>
<script src=" https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js " integrity=" sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo " crossorigin=" anonymous "></script>
<script src=" https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js " integrity=" sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/ " crossorigin=" anonymous "></script>
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>

</html>