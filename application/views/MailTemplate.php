<div class="card">
    <div class="card-body">
        <h3 class="card-title"><u>Contact Form To Mail</u></h3>
        <table class="table">
            <tbody>
            <tr>
                <td><b>Contact Name</b></td>
                <td><b>:</b></td>
                <td><?php echo $ContactName; ?></td>
            </tr>
            <tr>
                <td><b>Contact Email</b></td>
                <td><b>:</b></td>
                <td><?php echo $ContactEmail; ?></td>
            </tr>
            <tr>
                <td><b>Contact Phone</b></td>
                <td><b>:</b></td>
                <td><?php echo $ContactPhoneNumber; ?></td>
            </tr>
            <tr>
                <td><b>Subject</b></td>
                <td><b>:</b></td>
                <td><?php echo $ContactSubject; ?></td>
            </tr>
            <tr>
                <td><b>Message</b></td>
                <td><b>:</b></td>
                <td><?php echo $ContactMessage; ?></td>
            </tr>
            <tr>
                <td><b>Date</b></td>
                <td><b>:</b></td>
                <td><?php echo date("d.m.Y H:i", strtotime($ContactDate)); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

