<div class="container">
    <div class="row firstArea order-2">
        <div class="col-12 col-md-4 order-1 order-md-1 text-center text-md-start">
            <p class="hood">Have any Questions?</p>
        </div>
        <div class="col-6 col-md-4 order-2 order-md-2 text-center ">
            <div class="mailArea">
                <a href="mailto:info@aytim.se"><img src="<?php echo base_url('assets/img/icon/mail.png'); ?>" alt="">
                    info@aytim.se</a>
            </div>
        </div>
        <div class="col-6 col-md-4 order-3 order-md-3">
            <div class="telArea">
                <a href="tel:+4672-8538368">
                    <img src="<?php echo base_url('assets/img/icon/phone.png'); ?>" alt="">
                    +46 72-853 83 68</a>
            </div>
        </div>
    </div>
    <div class="row secondArea align-items-center pb-5 order-1">
        <div class="col-xxs-12 col-sm-6 col-md-4 order-2 order-md-1 logoAndTextArea text-center text-md-start">
            <a href="<?php echo base_url(); ?>">
                <img class="img-fluid" src="<?php echo base_url('assets/img/logo-white.png'); ?>" alt="">
            </a>
            <p>Vill du som företagare anställa någon
                från utanför EU? Har du som bor
                utanför.</p>
        </div>
        <div class="col-12 col-sm-12 col-md-4 order-1 order-md-2 text-center  d-flex d-md-block FooterMenu mb-2 text-lg-start">
            <p><a class="text-decoration-none" href="<?php echo base_url(); ?>">HOME</a></p>
            <p><a class="text-decoration-none" href="<?php echo base_url('about-us'); ?>">ABOUT US</a></p>
            <p><a class="text-decoration-none" href="<?php echo base_url('services'); ?>">OUR SERVICES</a></p>
            <p><a class="text-decoration-none" href="<?php echo base_url('contact-us'); ?>">CONTACT US</a></p>
        </div>
        <div class="col-12 col-sm-6 col-md-4 order-3  order-md-3 informationArea">
            <div class="align-items-center d-flex place-content-center">
                <div class=" p-0 locationArea">
                    <img class="locationImg" src="<?php echo base_url('assets/img/icon/footer-map-sweden.png'); ?>" alt="">
                </div>
                <div class="locationText ps-0">
                    <div class="d-flex align-items-center mb-3">
                        <img class="iconOne" src="<?php echo base_url('assets/img/icon/marker.png'); ?>" alt="">
                        <p>Fridensborgsvägen 133
                            170 62 Solna, İsveç
                        </p>
                    </div>
                    <div class="d-flex align-items-center">
                        <a href="mailto:info@aytim.se"> <img class="iconTwo" src="<?php echo base_url('assets/img/icon/mail.png'); ?>" alt="">
                            info@aytim.se</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>