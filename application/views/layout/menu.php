<?php if (!isset($menu_active)) {
    $menu_active = "home";
} ?>

<div class="headerBack d-none d-lg-block">
    <div class="container-fluid container-xll container-lg">
        <div id="info">
            <div class="contactOne">
                <div class="time" class="d-flex">
                    <img class="ps-0" src="<?php echo base_url('assets/img/icon/alarm_on.png'); ?>" alt="">
                    <p>Mon - Sat 9.00-18.00</p>
                </div>
                <div class="locate">
                    <img src="<?php echo base_url('assets/img/icon/marker.png'); ?>" alt="">
                    <p> Fridensborgsvägen 133 170 62 Solna, İsveç</p>
                </div>
            </div>
            <div class="contactTwo">
                <div class="mail me-3">
                    <img class="ps-0" src="<?php echo base_url('assets/img/icon/mail.png'); ?>" alt="">
                    <a href="mailto:info@aytim.se">info@aytim.se</a>
                </div>
                <div class="tel">
                    <div class="boxTwo">
                        <a href="tel:+4672-8538368"><img src="<?php echo base_url('assets/img/icon/phone.png'); ?>" alt="">
                            +4672-8538368</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box d-none d-xxl-block"></div>
</div>
<div class="container-fluid container-lg p-0 pl-lg-4">
    <nav class="navbar navbar-expand-lg pb-0">
        <div class="container-fluid p-0">
            <a href="<?php echo base_url(); ?>">
                <img class="mb-2 ms-3" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="">
            </a>
            <button class="navbar-toggler mb-2 me-3" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarNavAltMarkup">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 d-none d-lg-block">
                    <li class="nav-item text-center">
                        <p>Visa & Immigration Consultants</p>
                    </li>
                </ul>
                <form>
                    <div class="d-block d-lg-flex text-center text-lg-start mt-1 mb-3">
                        <li class="mt-2 ms-3 me-3"><a <?php echo $menu_active == "home" ? " class='active'" : "" ?> href="<?php echo base_url(); ?>">HOME</a></li>
                        <li class="mt-2 ms-3 me-3"><a <?php echo $menu_active == "aboutus" ? " class='active'" : "" ?> href="<?php echo base_url('about-us'); ?>">ABOUT US</a></li>
                        <li class="mt-2 ms-3 me-3"><a <?php echo $menu_active == "services" ? " class='active'" : "" ?> href="<?php echo base_url('services'); ?>">OUR SERVICES</a></li>
                        <li class="mt-2 ms-3 me-3"><a <?php echo $menu_active == "contactus" ? " class='active'" : "" ?> href="<?php echo base_url('contact-us'); ?>">CONTACT US</a></li>
                    </div>
                    <div class="infoTwo d-block d-lg-none">
                        <div class=" contactOne ps-4 ps-sm-5 align-items-center">
                            <div class="time" class="d-flex">
                                <img class="me-2 " src="<?php echo base_url('assets/img/icon/alarm_on.png'); ?>" alt="">
                                <p class="text-white">Mon - Sat 9.00-18.00</p>
                            </div>
                            <div class="locate">
                                <img class="me-2" src="<?php echo base_url('assets/img/icon/marker.png'); ?>" alt="">
                                <p class="text-white"> Fridensborgsvägen 133 170 62 Solna, İsveç</p>
                            </div>
                        </div>
                        <div class="contactTwo ps-4 ps-sm-5 ">
                            <div class="mail align-items-center">
                                <img class="me-2 " src="<?php echo base_url('assets/img/icon/mail.png'); ?>" alt="">
                                <a class="text-white" href="mailto:info@aytim.se">info@aytim.se</a>
                            </div>
                            <div class="tel">
                                <div class="boxTwo">
                                    <a class="text-white" href="tel:+4672-8538368"><img class="me-2" src="<?php echo base_url('assets/img/icon/phone.png'); ?>" alt="">
                                        +4672-8538368</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </nav>
</div>