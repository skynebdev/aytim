<?php
$menu_active = "home";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/titleLogo.png'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css.map'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/genel.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css?v=123'); ?>" />
    <script src='<?php echo base_url('assets/js/jquery-3.5.1.js'); ?>'></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.js'); ?>"></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <title>AYTIM</title>
</head>

<body>
    <div class="specialNav">
        <?php include("layout/menu.php"); ?>
    </div>
    <header>
        <div class="overly"></div>
        <video id="OracleVid" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source id="OracleVidSRC" type="<?php echo base_url('assets/video/mp4'); ?>">
        </video>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="line"></div>
                    <p class="hood">Vi hjälper dig genom <br>
                        hela processen
                    </p>
                    <p class="hoodTwo">Professionell och rättsäker rådgivning inom utlänningsrätt</p>
                    <div class="HeaderButtons">
                        <button class="BookOfButton"><a href="">Book of Consultation!</a></button>
                        <button class="ContactButton"><a href="<?php echo base_url('contact-us'); ?>">Contact Us</a></button>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="sectionOne">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-sm-9 col-md-7 col-lg-4 col-xl-4 h-100">
                    <div class="firstCard">
                        <div class="cardInside">
                            <div class="imgAndHood">
                                <img src="<?php echo base_url('assets/img/icon/business.png'); ?>" alt="">
                                <p>New Application <br>
                                    for a Work Permit</p>
                            </div>
                            <div class="text">
                                <p>We help both employers and employees
                                    throughout the process from application to
                                    decision. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-sm-9 col-md-7 col-lg-4 col-xl-4 h-100">
                    <div class="secondCard">
                        <div class="cardInside">
                            <div class="imgAndHood">
                                <img src="<?php echo base_url('assets/img/icon/visa_stamp.png'); ?>" alt="">
                                <p>New Application <br>
                                    Due to Connection
                                </p>
                            </div>
                            <div class="text">
                                <p>Nowadays, a maintenance requirement is
                                    imposed on you who live in Sweden and want
                                    to get your partner here.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-sm-9 col-md-7 col-lg-4 col-xl-4 h-100">
                    <div class="thirtCard">
                        <div class="cardInside">
                            <div class="imgAndHood ">
                                <img src="<?php echo base_url('assets/img/icon/passport.png'); ?>" alt="">
                                <p>General Advice</p>
                            </div>
                            <div class="text">
                                <p>We offer a meeting for general advice where we
                                    go through your case and your conditions for a
                                    residence permit</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sectionTwo">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 textArea mb-4">
                    <p class="hoodS">ABOUT CONSULTANTS</p>
                    <p class="hoodB">Why should you hire <br>
                        <span class="hoodBTwo">Aytim Rådgivning?</span>
                    </p>
                    <p class="text">Vill du som företagare anställa någon från utanför EU</p>
                    <p class="text">Har du som bor utanför EU hittat ett arbete i Sverige och vill flytta hit?</p>
                    <p class="text">Har du ett gällande arbetstillstånd och vill förlänga ditt tillstånd?</p>
                    <p class="text">Har du funnit kärleken utanför EU:s gränser?</p>
                    <p class="textTwo">I så fall har du kommit rätt. Vi hjälper dig under hela processen,
                        från ansökan till beslut.</p>
                    <p class="textThree">Du behöver bara underteckna en fullmakt så vi sköter resten.</p>
                    <p class="text">Vi hanterar all kommunikation med myndigheter såsom
                        Migrationsverket och svenska ambassader/konsulater runt om i
                        världen. Vi representerar både dig som företagare och dig som
                        privatperson. Vi stöttar dig och håller dig uppdaterad under hela
                        ansökningsprocessen.</p>
                </div>
                <div class="col-6 col-lg-3 text-md-center bigImage text-end">
                    <img class="img-fluid" src="<?php echo base_url('assets/img/imageOne.png'); ?>" alt="">
                </div>
                <div class="col-6 col-lg-3 text-md-center imagesArea text-end">
                    <img class="img-fluid" src="<?php echo base_url('assets/img/imageThree.png'); ?>" alt="">
                    <img class="img-fluid mt-4" src="<?php echo base_url('assets/img/imageTwo.png'); ?>" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="sectionThree">
        <div class="container textArea align-items-center">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <p class="hoodS">ABOUT CONSULTANTS</p>
                    <p class="hoodB">Detta kan vi <span class="hoodBTwo">hjälpa dig med</span></p>
                </div>
                <div class="col-12 col-md-6">
                    <p class="text">Vi hanterar all kommunikation med myndigheter såsom Migrationsverket och
                        svenska ambassader/konsulater runt om i världen. Vi representerar både dig som
                        företagare och dig som privatperson.</p>
                </div>
            </div>
        </div>
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-md-12 columns overflow-h">
                    <div class="owl-carousel owl-theme text-white">
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img class="sliderIcon" src="<?php echo base_url('assets/img/icon/11.svg'); ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">Rådgivning</h5>
                                    <a href="">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img class="sliderIcon" src="<?php echo base_url('assets/img/icon/22.svg'); ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">Ny ansökan om arbetstillstånd från utlandet</h5>
                                    <a href="">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img class="sliderIcon" src="<?php echo base_url('assets/img/icon/33.svg'); ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">Ansökan om arbetstillstånd efter att du fått avslag på din
                                        ansökan om asyl</h5>
                                    <a href="">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img class="sliderIcon" src="<?php echo base_url('assets/img/icon/44.svg'); ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">Ansökan om förlängning av
                                        ditt arbetstillstånd
                                    </h5>
                                    <a href="">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img class="sliderIcon" src="<?php echo base_url('assets/img/icon/55.svg'); ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">Ansökan om arbetstillstånd
                                        vid byte av yrke eller
                                        arbetsgivare
                                    </h5>
                                    <a href="">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img class="sliderIcon" src="<?php echo base_url('assets/img/icon/66.svg'); ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">Ansökan om arbetstillstånd
                                        vid byte av yrke eller
                                        arbetsgivare
                                    </h5>
                                    <a href="">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card" style="width: 18rem;">
                                <img class="sliderIcon" src="<?php echo base_url('assets/img/icon/77.svg'); ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">Översätta diverse
                                        handlingar</h5>
                                    <a href="">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="sectionFour">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="InfoCard row align-items-center">
                        <div class="col-12 col-md-9 text-center text-md-start">
                            <p class="hood m-0">Are You Looking For Visa Applications Just Call Us!</p>
                            <p class="hoodTwo m-0">Call us today 1-888-123-45678 or email us: info@domainname.com</p>
                        </div>
                        <div class="col-12 col-md-3 mt-3 mt-md-0 text-center">
                            <a href="<?php echo base_url('contact-us'); ?>">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container whatOurClientSays">
            <div class="row">
                <div class="col-12 col-lg-6 order-2 order-lg-1 p-0 whatOurClientSaysImg">
                    <img class="img-fluid" src="<?php echo base_url('assets/img/bigImage.png'); ?>" alt="">
                </div>
                <div class="col-12 col-lg-6 order-1  order-lg-2  whatOurClientSaysText bg-white p-4">
                    <div class="hoodArea">
                        <p class="hoodS">
                            TESTIMONIALS
                        </p>
                        <p class="hoodB">
                            What Our <span class="hoodBTwo">Client Says?</span>
                        </p>
                        <div class="textArea">
                            <p class="text">
                                I really would like to appreciate Tripzia and the
                                entire team, especially Ms Anandias Alex for
                                helping me get my student visa for the Brunel
                                University. She helped me all way to find the
                                University & right course. She was there for
                                me through the entire process.
                            </p>
                            <img src="<?php echo base_url('assets/img/MaskGroup3.png'); ?>" alt="">
                            <p class="name">Alan Sears</p>
                            <p class="place">Switzerland</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <?php include("layout/footer.php"); ?>
    </footer>
</body>
<script src=" https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js " integrity=" sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo " crossorigin=" anonymous "></script>
<script src=" https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js " integrity=" sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/ " crossorigin=" anonymous "></script>
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>

</html>