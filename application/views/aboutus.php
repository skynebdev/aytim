<?php
$menu_active = "aboutus";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/titleLogo.png'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css.map'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/genel.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css?v=123'); ?>" />
    <script src='<?php echo base_url('assets/js/jquery-3.5.1.js'); ?>'></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.js'); ?>"></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <title>About Us</title>
</head>

<body class="aboutUsMainArea">
    <div class="specialNav">
        <?php include("layout/menu.php"); ?>
    </div>
    <header class="aboutus">
        <div class="container">
            <div class="row">
                <div class="overly"></div>
                <div class="hoodArea">
                    <div class="container">
                        <div class="row">
                            <div class="line"></div>
                            <p class="hoodB">About Us <br> Aytim Radgiving</p>
                            <p class="hoodS">Professional and legally secure advice in aliens law</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="aboutUsSOne">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 text-start">
                    <img class="img-fluid" src="<?php echo base_url('assets/img/aboutUsimg.png'); ?>" alt="">
                </div>
                <div class="col-12 col-md-7 ps-3">
                    <div class="textArea">
                        <p class="hoodS">WHY CHOOSE US</p>
                        <p class="hoodB">About Us</p>
                        <p class="hoodBTwo">Aytim Rådgivning?</p>
                        <p class="text">Vill du som företagare anställa någon från utanför EU? Har du som
                            bor utanför EU hittat ett arbete i Sverige och vill flytta hit? Har du ett
                            gällande arbetstillstånd och vill förlänga ditt tillstånd? Har du funnit
                            kärleken utanför EU:s gränser? I så fall har du kommit rätt. Vi hjälper
                            dig under hela processen, från ansökan till beslut.
                        </p>
                        <p class="textTwo">Du behöver bara underteckna en fullmakt så vi sköter resten.</p>
                        <p class="text">Vi hanterar all kommunikation med myndigheter såsom
                            Migrationsverket och svenska ambassader/konsulater runt om i
                            världen. Vi representerar både dig som företagare och dig som
                            privatperson. Vi stöttar dig och håller dig uppdaterad under hela
                            ansökningsprocessen.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="aboutUsSTwo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 firstArea">
                    <div class="textArea">
                        <p class="hoodOne">Why should you hire
                            <span class="hoodBold">Aytim Rådgivning?</span>
                        </p>
                        <p class="text">Jo, för att vi gör ord till handling. Vi har omfattande erfarenhet och
                            kunskap inom utlänningsrätt. Som privatperson eller företagare är
                            processen för uppehålls- och arbetstillstånd både tids- och
                            kompetenskrävande. </p>
                        <p class="text"> Små felsteg längst vägen kan kosta dig dyrt och leda till att
                            uppehållstillstånd inte beviljas. Om processen däremot hanteras rätt
                            från början ökar chanserna att handläggningstiden kortas ner och att
                            en ansökan resulterar i ett uppehållstillstånd.</p>
                        <button><a href="<?php echo base_url('services'); ?>"> Aytim Our Services <img class="img-fluid" src="<?php echo base_url('assets/img/icon/play.png'); ?>" alt=""></a></button>
                    </div>
                </div>
                <div class="col-12 col-md-6 secondArea">
                    <div class="blueArea">
                        <p>”Jag har en gedigen
                            erfarenhet av
                            utlänningsrätt och min
                            ambition är att föra din
                            talan och förenkla din
                            process om att få ett
                            uppehållstillstånd i
                            Sverige.”</p>
                        <p class="name">Ibrahim Unal</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="staffArea">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xxl-4  hoodArea">
                    <p class="hoodS">SKILLFUL PROFESSIONALS</p>
                    <p class="hoodB">Meet Our</p>
                    <p class="hoodBTwo">Dedicated Team!</p>
                </div>
                <div class="col-sm-6 col-lg-4 col-xxl-2 offset-xxl-1">
                    <div class="imgArea">
                        <img class="img-fluid" src="<?php echo base_url('assets/img/ibrahim.png'); ?>" alt="">
                        <p class="position">Ombud – Migrationsrådgivare</p>
                        <p class="name">Ibrahim Unal</p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xxl-2 offset-xxl-1">
                    <div class="imgArea">
                        <img class="img-fluid" src="<?php echo base_url('assets/img/shadi.png'); ?>" alt="">
                        <p class="position">Administrativ Assistent</p>
                        <p class="name">Shadabeh Mirzazadeh</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <?php include("layout/footer.php"); ?>
    </footer>
</body>
<script>
    document.getElementById('iframe').style.zIndex = 1;
</script>
<script src=" https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js " integrity=" sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo " crossorigin=" anonymous "></script>
<script src=" https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js " integrity=" sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/ " crossorigin=" anonymous "></script>
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>

</html>