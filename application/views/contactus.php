<?php
$menu_active = "contactus";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/titleLogo.png'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css.map'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/genel.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css?v=123'); ?>" />
    <script src='<?php echo base_url('assets/js/jquery-3.5.1.js'); ?>'></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.js'); ?>"></script>
    <script rel="stylesheet" src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <title>Contact Us</title>
</head>

<body>
    <div class="specialNav">
        <?php include("layout/menu.php"); ?>
    </div>
    <header class="contactUs">
        <div class="hoodArea">
            <div class="container">
                <div class="row">
                    <div class="line"></div>
                    <p class="hoodB">Contact Us <br> Aytim Radgiving</p>
                    <p class="hoodS">Professional and legally secure advice in aliens law</p>
                </div>
            </div>
        </div>
    </header>
    <section class="location">
        <iframe id="iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2032.0260155679978!2d17.981106316156268!3d59.38259301379278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x465f9dd4654319bd%3A0x443a573302a8054!2zRnJpZGVuc2JvcmdzdsOkZ2VuIDEzMywgMTcwIDYyIFNvbG5hLCDEsHN2ZcOn!5e0!3m2!1str!2str!4v1637675223639!5m2!1str!2str" frameborder="0" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </section>
    <section class="formArea">
        <div class="container" id="movedown">
            <div class="row" >
                <div class="col-sm-12  col-lg-4 col-xxl-3">
                    <div class="firstArea">
                        <p class="hoodB">Our Location</p>
                        <p class="text">Aytim Rådgivning AB (Org. nr: 559301-0324) Fridensborgsvägen 133 170 62 Solna, Sverige</p>
                        <p class="hoodB mt-1 mt-lg-0">Quick Contact</p>
                        <p class="text">E:mail: <a href="mailto:info@aytim.se"></a>info@aytim.se</p>
                    </div>
                    <div class="secondArea">
                        <p>We will get back to you within 24 hours, or call us everyday, 9.00 AM - 12:00 PM</p>
                        <div class="d-flex align-items-center justify-content-center">
                            <img src="<?php echo base_url('assets/img/icon/contactPhone.png'); ?>" alt="">
                            <a href="tel:+4672-85368">+46 72-853 83 68</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xxl-9 ps-0 ps-lg-3">
                    <div class="formAreaTwo">
                        <div class="hoodArea">
                            <p class="hoodS">WHY CHOOSE US</p>
                            <p class="hoodB">GET IN <span class="hoodBTwo">TOUCH ?</span></p>
                        </div>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-success">
                                <?= $this->session->flashdata('success'); ?>
                            </div>
                        <?php }elseif ($this->session->flashdata('errors')) { ?>
                            <div class="alert alert-danger">
                                <?= $this->session->flashdata('errors'); ?>
                            </div>
                        <?php }else { }?>

                        <form action="<?php echo base_url('ContactUs/dataInsertDB'); ?>" method="post">
                            <div class="inputArea">
                                <input type="text" name="contact_name"  placeholder="Your Name" required>
                                <input type="email" name="contact_email" placeholder="Your Email" required>
                            </div>
                            <div class="inputArea">
                                <input type="tel" name="contact_phone" placeholder="Phone Number" required>
                                <input type="text" name="contact_subject" placeholder="Subject" required>
                            </div>
                            <div class="mailArea">
                                <textarea name="contact_message" placeholder="Your Message" required></textarea>
                            </div>
                            <div class="buttonArea">
                                <button type="submit">Submit Request !</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contactUsArea">
        <div class="overly"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-9 textArea">
                    <p class="hoodB">Are You Looking For Visa Applications Just Call Us!</p>
                    <p class="hoodS">Call us today <span> <a href="tel:1-888-45678">1-888-45678 </a></span>or Email us: <a href="mailto:info@domainname.com">info@domainname.com</a></p>
                </div>
                <div class="col-12 col-lg-3 contactButtonArea">
                    <a href="">Contact Us</a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <?php include("layout/footer.php"); ?>
    </footer>
</body>
<script>
    document.getElementById('iframe').style.zIndex = 1;
</script>
<script src=" https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js " integrity=" sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo " crossorigin=" anonymous "></script>
<script src=" https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js " integrity=" sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/ " crossorigin=" anonymous "></script>
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>

</html>