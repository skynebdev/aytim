<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactModel extends CI_Model {

    public function addData($data,$table)
    {
        return $this->db->insert($table,$data);
    }


    public function updateData($where,$data,$table)
    {
        return $this->db->where($where)->update($table,$data);
    }




}
