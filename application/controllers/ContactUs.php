<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactUs extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('ContactModel');
    }

    public function index()
	{
		$this->load->view('contactus');

	}

    public function formSendMail($formData = array())
    {

        $fromMail = "emrah@skyneb.com";
        $fromName = "Emrah A.";
        $toMail =  "emrah@skyneb.com";


        $this->email->initialize(MAIL_CONFIG);
        $message = $this->load->view('MailTemplate',$formData,true);

        $this->email->from($fromMail, $fromName);
        $this->email->to($toMail);

        $this->email->subject($formData['ContactSubject']);
        $this->email->message($message);
        if( !$this->email->send())
        {
            return -1;
        }
        else
        {
            return 1;
        }


    }


    public function dataInsertDB()
    {

        if($this->input->method() == "post"){

            $this->form_validation->set_rules('contact_name', 'Name', 'required|trim');
            $this->form_validation->set_rules('contact_email','Email', 'required|trim|valid_email');
            $this->form_validation->set_rules('contact_phone','Phone Number', 'required|trim');
            $this->form_validation->set_rules('contact_subject','Subject', 'required|trim');
            $this->form_validation->set_rules('contact_message','Message', 'required|trim');


                    if($this->form_validation->run() == FALSE){

                        $this->session->set_flashdata('errors', validation_errors('* '));
                        redirect(base_url('contactus#movedown'));

                    }else{

                            $formData = array(
                                'ContactName'       => strip_tags(trim($this->input->post('contact_name', true))),
                                'ContactEmail'      => strip_tags(trim($this->input->post('contact_email', true))),
                                'ContactPhoneNumber'=> strip_tags(trim($this->input->post('contact_phone', true))),
                                'ContactSubject'    => strip_tags(trim($this->input->post('contact_subject', true))),
                                'ContactMessage'    => strip_tags(trim($this->input->post('contact_message', true))),
                                'ContactDate'       => date("Y-m-d H:i:s"),
                                'ContactMailStatus' => 0

                            );

                            $addQuery = $this->ContactModel->addData($formData, 'contacts');

                            $sendQuery = $this->formSendMail($formData);

                            if($addQuery && $sendQuery==1){

                                $updateArray = array(
                                  'ContactMailStatus' => 1
                                );

                                $updateQuery = $this->ContactModel->updateData($formData,$updateArray,'contacts');

                                $this->session->set_flashdata('success', 'Your message has been sent successfully..');
                                redirect(base_url('contactus#movedown'));


                            }else{

                                $this->session->set_flashdata('success', 'Your message has been created successfully..');
                                redirect(base_url('contactus#movedown'));

                            }


                    }

        }


    }







}
