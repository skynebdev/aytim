<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $this->load->view('index');
    }
    public function About()
    {
        $this->load->view('aboutus');
    }
    public function ContactUs()
    {
        $this->load->view('contactus');
    }
    public function Services()
    {
        $this->load->view('services');
    }
}
